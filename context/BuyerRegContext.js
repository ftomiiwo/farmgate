import {createContext, useState} from 'react';

export const BuyerRegContext = createContext();

export const BuyerProvider =(props)=>{
    const [phoneNo, setPhoneNo] = useState("");
    const [input, setInput] = useState("");
    const [secInput, setSecInput] = useState("");
    const [region, setRegion] = useState("");
    const [city, setCity] = useState("");
    const [postalCode, setPostalCode] = useState("");
    const [address, setAddress] = useState("");
    return(
        <BuyerRegContext.Provider value={[phoneNo, setPhoneNo, input, setInput, secInput, setSecInput, region, setRegion, city, setCity, postalCode, setPostalCode, address, setAddress]}>
            {props.children}
        </BuyerRegContext.Provider>
    )
}