import {createContext, useState} from 'react';

export const FinancierRegContext = createContext();

export const FinancierRegProvider =(props)=>{
    const [phoneNo, setPhoneNo] = useState("");
    const [organisationName, setOrganisationName] = useState("");
    const [interest, setInterest] = useState("");
    const [ funderType, setFunderType] = useState("");
    const [ termsRoi, setTermsRoi] = useState("");
    // const [postalCode, setPostalCode] = useState("");
    // const [address, setAddress] = useState("");
    return(
        // <FarmerRegContext.Provider value={[phoneNo, setPhoneNo, input, setInput, secInput, setSecInput, region, setRegion, city, setCity, postalCode, setPostalCode, address, setAddress]}>
        <FinancierRegContext.Provider value={[phoneNo, setPhoneNo, organisationName, setOrganisationName, interest, setInterest, funderType, setFunderType, termsRoi, setTermsRoi]}>
            {props.children}
        </FinancierRegContext.Provider>
    )
}