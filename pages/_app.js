import '../styles/globals.css'
import '../styles/index.css'
import "../styles/progress_form.css";
import "../styles/sass/farmgate.scss";

function MyApp({ Component, pageProps }) {
  const Layout = Component.Layout || emptyNavLayout;
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
    )
}
// without navbar 
const emptyNavLayout = ({children})=><>{children}</>;

export default MyApp
