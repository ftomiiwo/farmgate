import Link from 'next/link';
const RegionDetails = ({regionClass}) =>{
    return(
        <> 
        {/* details_grid must have numbers attached */}
            <div className={"details_grid details_grid1 " + regionClass}>
                <div className="items_">
                    <h5>Farmer</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Financiers</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Input Providers</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Buyers</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
            </div>
            {/* second redion  */}
            <div className={"details_grid details_grid2 " + regionClass}>
                <div className="items_">
                    <h5>Farmer</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="/#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Financiers</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Input Providers</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
                <div className="items_">
                    <h5>Buyers</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div>
                        <Link href="#">
                            <a>Learn More</a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}
export default RegionDetails;