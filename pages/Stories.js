import Carousel from "react-multi-carousel";
import Link from "next/link";
import "react-multi-carousel/lib/styles.css";

const Stories = ()=>{
    const responsive = {
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 4,
          slidesToSlide: 1 // optional, default to 1.
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 3,
          slidesToSlide: 1 // optional, default to 1.
        },
        tablet: {
            breakpoint: { max: 767, min: 464 },
            items: 2,
            slidesToSlide: 1 // optional, default to 1.
          },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1,
          slidesToSlide: 1 // optional, default to 1.
        }
      };
    return(
        <>
            <Carousel
            swipeable={false}
            draggable={true}
            showDots={false}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            // autoPlay={this.props.deviceType !== "mobile" ? true : false}
            autoPlay={true}
            autoPlaySpeed={1000}
            keyBoardControl={true}
            customTransition="all 0.5 ease-in-out"
            // customTransition="transform 300ms ease-in-out"
            transitionDuration={5000}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["desktop", "tablet", "mobile"]}
            // deviceType={this.props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
            >
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                   <div className="user_img">
                       <img src="/images/profile.jpg" alt="User"/>
                   </div>
                    <p>rdsLorem ipsum dolor sit amet, eitn consectetur adipiscing elit. Oci et nibh felis nisl nulla viverra facilisi. </p>
                    <p className="writer_">Danny Rain</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/profile.jpg" alt="User"/>
                   </div>
                    <p>teLorem ipsum dolor sit amet, eitn consectetur adipiscing elit. Oci et nibh felis nisl nulla viverra facilisi. </p>
                    <p className="writer_">Danny Rain</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/profile.jpg" alt="User"/>
                   </div>
                    <p>reLorem ipsum dolor sit amet, eitn consectetur adipiscing elit. Oci et nibh felis nisl nulla viverra facilisi. </p>
                    <p className="writer_">Danny Rain</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/profile.jpg" alt="User"/>
                   </div>
                    <p>eLorem ipsum dolor sit amet, eitn consectetur adipiscing elit. Oci et nibh felis nisl nulla viverra facilisi. </p>
                    <p className="writer_">Danny Rain</p>
               </div>
               <div className="story_item mx-2 md:mr-3 md:ml-0">
                    <div className="user_img">
                       <img src="/images/profile.jpg" alt="User"/>
                   </div>
                    <p>wLorem ipsum dolor sit amet, eitn consectetur adipiscing elit. Oci et nibh felis nisl nulla viverra facilisi. </p>
                    <p className="writer_">Danny Rain</p>
               </div>
            </Carousel>
        </>
    )
}
export default Stories;