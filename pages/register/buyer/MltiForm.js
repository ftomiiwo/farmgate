import React from "react";
import { useForm, useStep } from "react-hooks-helper";
import {useContext} from 'react';
import {BuyerRegContext} from '../../../context/BuyerRegContext';
import Details from "./Details";
import Address from "./Address";
import Review from "./Review";
// import Submit from "./Submit";

// import "./s|"?tyles.css";
// npm i react-hooks-helper

const steps = [
  { id: "details" },
  { id: "address" },
  { id: "review" },
  // { id: "submit" }
];

const defaultData = {
  firstName: "",
  lastName: "",
  email: "",
};

const MultiForm = ({ images, changeStateOne, changeStateTwo, changeStateThree}) => {
  const [formData, setForm] = useForm(defaultData);
  const [phoneNo, setPhoneNo, input, setInput, secInput, setSecInput, region, setRegion, city, setCity, postalCode, setPostalCode, address, setAddress] = useContext(BuyerRegContext);
  const { step, navigation } = useStep({ initialStep: 0, steps });
  const { id } = step;
  const props = { formData, setForm, phoneNo, setPhoneNo, input, setInput, secInput, setSecInput, region, setRegion, city, setCity, postalCode, setPostalCode, address, setAddress, navigation, changeStateOne, changeStateTwo,changeStateThree };
  // console.log(changeState);
  // console.log(props);
  switch (id) {
    case "details":
      return <Details {...props} />;
    case "address":
      return <Address {...props} />;
    case "review":
      return <Review {...props} />;
    // case "submit":
    //   return <Submit {...props} />;
    default:
      return null;
  }
};
export default MultiForm;