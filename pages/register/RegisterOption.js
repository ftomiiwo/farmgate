import {useState, useEffect} from 'react';
import {useRouter} from 'next/router';
import Head from 'next/head';
import Link from 'next/link';

const RegisterOption = () =>{
    let router = useRouter();
    const [selectedRegOption, setSelectedRegOption] = useState("farmer")
    const selectOption = (e) =>{
        e.preventDefault();
        let selectedForm = selectedRegOption;
        // select the page to display 
        switch(selectedForm){
            case "farmer" :
                return router.push('/register/FarmerReg');
                // return router.push('/register/FarmerReg', 'farmer');
            case "inputProvider" :
                return  router.push('/register/InputProvidersReg');
            case "financier" :
                return  router.push('/register/FinanciersReg');
            case "agroBuyer": 
                return  router.push('/register/AgroBuyersReg');
            case "agroSeller" :
                return  router.push('/register/AgroSellersReg');
                // return  router.push('/register/AgroSellersReg', 'agroSeller');
            default: 
                return null;

        }
    }
    return(
        <>
          <Head>
                <title> Registration options - Farmgate</title>
                <link rel="icon" href="/farmgate_favicon.ico" />
                {/* <link
                    rel="preload"
                    href="/fonts/stylesheet.css"
                    as="style"
                    crossOrigin=""
                /> */}
            </Head>
           <div className="auth_body">
           <header className="auth_header px-4 md:px-16 pt-4 md:pt-10 pb-6">
                <Link href="/">
                <a className="back_btn align-center">
                    <svg className="inline-block mr-2" width="22" height="17" viewBox="0 0 22 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.5 1L1 9.5M1 9.5L9.5 16.5M1 9.5H11.25H21.5" stroke="#B1B1B1"/>
                    </svg>

                    Return to the main page</a>
                </Link>
                <div className="text-center">
                    <Link href="/">
                        <a>
                        <img className="inline-block" src="/images/farmgate_logo.svg" alt="Farmgate logo"/>
                        </a>
                    </Link>
                </div>
            </header>
            <section className="footer_container">
                <div className="text-center options_header">
                    <h3>Grow with Farmgate</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ipsum magna, egestas et orci nec, dignissim ullamcorper lorem.
Nam ac semper est, at faucibus massa. Phasellus eu elit id nibh accumsan tempus sit amet quis erat. Ut porttitor facilisis nulla et elementum.</p>
                </div>
                <div className="form_container pt-10 md:pt-16">
                    <form onSubmit={selectOption}>
                        <div className="input_grid">
                            <div className="input_item">
                                <input type="radio" defaultChecked name="regOption" value="farmer" id="farmerReg" onChange={(e)=>(setSelectedRegOption(e.target.value))}/> 
                                <label htmlFor="farmerReg" className="shadow">
                                    <div className="pb-8">
                                        <img className="inline-block" src="/images/farmer.svg" alt="farmer"/>
                                    </div>
                                    <div className="card_body">
                                        <h5>Farmers</h5>
                                        <p>Farming Cooperatives, Associations, Cooperates and Individual Farmers</p>
                                    </div>
                                     {/* check button  */}
                                     <div className="card_footer">
                                        <span></span>
                                    </div>
                                </label>
                            </div>
                            <div className="input_item">
                                <input type="radio" name="regOption" value="inputProvider" id="inputProvider" onChange={(e)=>(setSelectedRegOption(e.target.value))}/> 
                                <label htmlFor="inputProvider" className="shadow">
                                    <div className="pb-8">
                                        <img className="inline-block" src="/images/inputProviders.svg" alt="Input Provider"/>
                                    </div>
                                    <div className="card_body">
                                        <h5>Input Providers</h5>
                                        <p>Irrigation, Mechanization, Seeds, Agro-Chemicals, Fertilizers, Insurance, Land owners, Extension services and Training.</p>
                                    </div>
                                     {/* check button  */}
                                     <div className="card_footer">
                                        <span></span>
                                    </div>
                                </label>
                            </div>
                            <div className="input_item">
                                <input type="radio" name="regOption" value="financier" id="financier" onChange={(e)=>(setSelectedRegOption(e.target.value))}/> 
                                <label htmlFor="financier" className="shadow">
                                    <div className="pb-8">
                                        <img className="inline-block" src="/images/financiers.svg" alt="financier"/>
                                    </div>
                                    <div className="card_body">
                                        <h5>Financiers</h5>
                                        <p>Development partners, Banks, Donors, Investment Cooperatives &amp; Club, Insurance companies.</p>
                                    </div>
                                     {/* check button  */}
                                     <div className="card_footer">
                                        <span></span>
                                    </div>
                                </label>
                            </div>
                            <div className="input_item">
                                <input type="radio" name="regOption" value="agroBuyer" id="agroBuyer" onChange={(e)=>(setSelectedRegOption(e.target.value))}/> 
                                <label htmlFor="agroBuyer" className="shadow"> 
                                    <div className="pb-8">
                                        <img className="inline-block" src="/images/buyer.svg" alt="buyer"/>
                                    </div>
                                    <div className="card_body">
                                        <h5>Agro Buyers</h5>
                                        <p>Processors, Aggregators, Retailers and Wholesalers.</p>
                                    </div>
                                     {/* check button  */}
                                     <div className="card_footer">
                                        <span></span>
                                    </div>
                                </label>
                            </div>
                            <div className="input_item">
                                <input type="radio" name="regOption" value="agroSeller" id="agroSeller" onChange={(e)=>(setSelectedRegOption(e.target.value))}/> 
                                <label htmlFor="agroSeller" className="shadow"> 
                                    <div className="pb-8">
                                        <img className="inline-block" src="/images/buyer.svg" alt="buyer"/>
                                    </div>
                                    <div className="card_body">
                                        <h5>Agro Sellers</h5>
                                        <p>Individual Farmers, Farmer Cooperatives and Associations. </p>
                                    </div>
                                    {/* check button  */}
                                    <div className="card_footer">
                                        <span></span>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div className="opt_btn text-center">
                            <button className="py-3 md:py-4 px-10">Register</button>
                            <p className="footer_p">Complete  your registration in  5 easy steps</p>

                        </div>
                    </form>
                </div>
            </section>
           </div>
            
        </>
    )
}
export default RegisterOption;