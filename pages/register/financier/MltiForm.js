import React from "react";
import { useForm, useStep } from "react-hooks-helper";
import {useContext} from 'react';
import {FinancierRegContext} from '../../../context/FinancierRegContext';
import Basic from "./Basic";
import Details from "./Details";
import Review from "./Review";
// import Submit from "./Submit";

// import "./s|"?tyles.css";
// npm i react-hooks-helper

const steps = [
  { id: "basic" },
  { id: "details" },
  { id: "review" },
  // { id: "submit" }
];

const defaultData = {
  firstName: "",
  lastName: "",
  email: "",
};

const MultiForm = ({ images, changeStateOne, changeStateTwo, changeStateThree}) => {
  const [formData, setForm] = useForm(defaultData);
  const [phoneNo, setPhoneNo, organisationName, setOrganisationName, interest, setInterest, funderType, setFunderType, termsRoi, setTermsRoi, region, setRegion, city, setCity, postalCode, setPostalCode, address, setAddress] = useContext(FinancierRegContext);
  const { step, navigation } = useStep({ initialStep: 0, steps });
  const { id } = step;
  const props = { formData, setForm, phoneNo, setPhoneNo, organisationName, interest, setOrganisationName, funderType, termsRoi, navigation, changeStateOne, changeStateTwo,changeStateThree };
  // console.log(changeState);
  // console.log(props);
  switch (id) {
    case "basic":
      return <Basic {...props} />;
    case "details":
      return <Details {...props} />;
    case "review":
      return <Review {...props} />;
    // case "submit":
    //   return <Submit {...props} />;
    default:
      return null;
  }
};
export default MultiForm;