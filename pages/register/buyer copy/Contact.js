import React from "react";

import ItemForm from "./ItemForm";

const Contact = ({ setForm, formData, navigation , changeStateTwo, changeStateThree}) => {
  const { phone, email } = formData;
  // console.log(prop);

  const { previous, next } = navigation;
  const prevFunction = () =>{
    changeStateTwo("_35Ago");
    changeStateThree("");

    previous();
  }
const nextFunction = () =>{
  changeStateThree("_2ZUAI");
  // changeStateOne("");
  next();
}
  return (
    <div className="form">
      <h3>Contact </h3>
      <ItemForm label="Phone" name="phone" value={phone} onChange={setForm} />
      <ItemForm label="E-mail" name="email" value={email} onChange={setForm} />
      <div>
        <button onClick={prevFunction}>Previous</button>
        <button onClick={nextFunction}>Next</button>
      </div>
    </div>
  );
};

export default Contact;