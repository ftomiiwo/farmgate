import React from "react";

import ItemForm from "./ItemForm";
import StateDrop from "./StateDrop";

const Address = ({ setForm, formData, navigation,changeStateOne,changeStateTwo,changeStateThree}) => {
  const { address, city, state, zip } = formData;

  const { previous, next } = navigation;
  const prevFunction = () =>{
    changeStateOne("_35Ago");
    changeStateTwo("");

    previous();
  }
  const nextFunction = () =>{
    changeStateTwo("_2ZUAI");
    changeStateThree("_35Ago");
    // changeStateOne("");
    next();
  }

  return (
    <div className="form_step">
      <h3>Address</h3>
      <ItemForm
        label="Address"
        name="address"
        value={address}
        onChange={setForm}
      />
      <ItemForm label="City" name="city" value={city} onChange={setForm} />
      <StateDrop label="State" name="state" value={state} onChange={setForm} />
      <ItemForm label="Zip" name="zip" value={zip} onChange={setForm} />
      <div className="flex justify-between my-8">
        <button className="backBtn_" onClick={previous}>
          <svg className="inline-block mr-2 relative" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.31906 3.92252C8.56875 3.6847 8.96469 3.69408 9.2025 3.94408C9.44063 4.19408 9.43063 4.5897 9.18094 4.82783L4.38656 9.37502H16.875C17.22 9.37502 17.5 9.65502 17.5 10C17.5 10.345 17.22 10.625 16.875 10.625H4.40656L9.18094 15.1725C9.43094 15.4106 9.44063 15.8063 9.2025 16.0563C9.07969 16.185 8.915 16.25 8.75 16.25C8.595 16.25 8.44 16.1928 8.31906 16.0775L2.86625 10.8838C2.63 10.6478 2.5 10.3341 2.5 10C2.5 9.66595 2.63 9.3522 2.87719 9.10564L8.31906 3.92252Z" fill="white"/>
          </svg>

          Previous
          </button>
        <button className="nextBtn" onClick={next}>Next
          <svg className="inline-block ml-2 relative" width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.68094 0.922515C9.43125 0.684703 9.03531 0.694078 8.7975 0.944078C8.55937 1.19408 8.56937 1.5897 8.81906 1.82783L13.6134 6.37502H1.125C0.78 6.37502 0.5 6.65502 0.5 7.00002C0.5 7.34501 0.78 7.62501 1.125 7.62501H13.5934L8.81906 12.1725C8.56906 12.4106 8.55937 12.8063 8.7975 13.0563C8.92031 13.185 9.085 13.25 9.25 13.25C9.405 13.25 9.56 13.1928 9.68094 13.0775L15.1337 7.88376C15.37 7.64783 15.5 7.33408 15.5 7.00002C15.5 6.66595 15.37 6.3522 15.1228 6.10564L9.68094 0.922515Z" fill="white"/>
          </svg>
        </button>
      </div>
    </div>
  );
};

export default Address;