import React from "react";

const states = [
  ["NSW", "New South Wales"],
  ["VIC", "Victoria"],
  ["WA", "Western Australia"]
];

const StateDrop = ({ label, ...others }) => (
  <>
    <label>{label}</label>
    <select {...others}>
      {states.map(([value, name], index) => (
        // console.log(value)
        // Object.keys(value[0];
         <option key={index} value={value}>{name}</option>
       
      ))}
    </select>
  </>
);

export default StateDrop;