import React from "react";
import { useForm, useStep } from "react-hooks-helper";
import {useContext} from 'react';
import {BuyerRegContext} from '../../../context/BuyerRegContext';
import Details from "./Details";
import Address from "./Address";
import Contact from "./Contact";
import Review from "./Review";
import Submit from "./Submit";

// import "./s|"?tyles.css";
// npm i react-hooks-helper

const steps = [
  { id: "details" },
  { id: "address" },
  { id: "contact" },
  { id: "review" },
  { id: "submit" }
];

const defaultData = {
  firstName: "",
  lastName: "",
  nickName: "Jan",
  address: "200 South Main St",
  city: "Anytown",
  state: "CA",
  zip: "90505",
  email: "email@domain.com",
  phone: ""
};

const MultiForm = ({ images, changeStateOne, changeStateTwo, changeStateThree}) => {
  const [formData, setForm] = useForm(defaultData);
  const [phoneNo, setPhoneNo, input, setInput, secInput, setSecInput] = useContext(BuyerRegContext);
  const { step, navigation } = useStep({ initialStep: 0, steps });
  const { id } = step;
  const props = { formData, setForm, phoneNo, setPhoneNo, input, setInput, secInput, setSecInput, navigation, changeStateOne, changeStateTwo,changeStateThree };
  // console.log(changeState);
  // console.log(props);
  switch (id) {
    case "details":
      return <Details {...props} />;
    case "address":
      return <Address {...props} />;
    case "contact":
      return <Contact {...props} />;
    case "review":
      return <Review {...props} />;
    case "submit":
      return <Submit {...props} />;
    default:
      return null;
  }
};
export default MultiForm;