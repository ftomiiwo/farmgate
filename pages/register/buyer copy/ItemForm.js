import React from "react";

const ItemForm = ({ label, children, type, ...otherProps }) => {
  // console.log(children)
  return (
      <div className="form_container flex-grow">
      {/* {type === "text" ? (
        <>
          <label>{label}</label>
          <input type={type} {...otherProps} />
        </>
      ) : (
        <>
          <label />
          <input type={type} {...otherProps} />
          {label}
        </>
      )} */}
       <>
          <label className="block">{label}</label>
          <input className="w-full" type={type}  placeholder="Doe"  {...otherProps} />
          {/* <label className="block text-sm text-gray-900">{label}</label> */}
          {/* <input type={type}  className= "bg-white w-full my-3 mt-0 p-2 border-gray-400 duration-200 focus:duration-200 border text-sm text-gray-800 rounded-md rounded focus:outline-none focus:border-blue-600 transition focus:transition" {...otherProps} /> */}
        </>
    </div>
  );
}

export default ItemForm;