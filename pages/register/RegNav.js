import Link from 'next/link';
import {useRouter} from 'next/router';
const RegNav  = () =>{
    const router = useRouter();
    return(
        <header className="auth_header px-4 md:px-16 pt-4 md:pt-10 pb-6">
            <Link href="/#">
            <a  onClick={(e) =>{e.preventDefault(); router.back()}} className="back_btn align-center">
                <svg className="inline-block mr-2" width="22" height="17" viewBox="0 0 22 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9.5 1L1 9.5M1 9.5L9.5 16.5M1 9.5H11.25H21.5" stroke="#B1B1B1"/>
                </svg>

                Return previous page</a>
            </Link>
            <div className="text-center">
                <Link href="/">
                    <a>
                    <img className="inline-block" src="/images/farmgate_logo.svg" alt="Farmgate logo"/>
                    </a>
                </Link>
            </div>
        </header>
    )
}
export default RegNav;
