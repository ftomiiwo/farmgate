import Head from 'next/head'
import styles from '../styles/Home.module.css'
import NavLayout from '../components/navLayout';
import HeroSlider from './HeroSlider';
import Possibilities from './Possibilities';
import Region from './Region';
import Stories from './Stories';
import Partners from './Partners';


export default function Home() {
  return (
    <>
     <Head>
        <title>Farmgate</title>
        <link rel="icon" href="/farmgate_favicon.ico" />
        {/* <link
            rel="preload"
            href="/fonts/stylesheet.css"
            as="style"
            crossOrigin=""
          /> */}
      </Head>
      <section>
        <HeroSlider/>
      </section>
      <section style={{background:"#fefefe"}}>
        <div className=" footer_container container_pad">
          <div className="section_head head_container py-6 md:py-10">
            <h3>World of <span>Endless Possibilities</span></h3>
            <p>No matter what your requirement is in the food value chain, we have a solution for you</p>
          </div>
        </div>
       
      </section>
      <div className="possibilities">
         <div className="footer_container container_pad">
          <Possibilities/>
         </div>
      </div>
      <section>
        <Region/>
      </section>
      <section className="stories_container">
        <div className="head_container stories_head footer_container container_pad">
          <h3>Success <span>Stories</span></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div className="footer_container container_pad">
            <Stories/>
          </div>
      </section>
      <section className="partners_section">
        <div className="head_container footer_container text-center container_pad">
          <h3>Our  <span>Partners</span></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div className="partner_container footer_container container_pad">
          <Partners/>
        </div>
      </section>
      <section className="seo_bg">
          <div className="head_container footer_container container_pad">
            <h3>Farmgate</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
          </div>
      </section>
      <footer className="text-center footer_container">
        <p>© Farmgate 2021 - A product of Farmcrowdy. All rights reserved.</p>
      </footer>
    </>
  )
}
Home.Layout = NavLayout;
