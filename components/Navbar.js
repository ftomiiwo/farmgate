import {useState} from 'react';
import Link from 'next/link';
import {useRouter} from 'next/router';

export default function Navbar() {
  // nav menu toggle 
  const [navbarOpen, setNavbarOpen] = useState(false);
  const navToggle = ()=>{
    setNavbarOpen(!navbarOpen);
  }
  // console.log(navbarOpen)
  // dropdown toggle 
  const[navOne, setNavOne] = useState(true);
  const[navTwo, setNavTwo] = useState(true);
  const closeNavDropdown =()=>{
    setNavOne(true);
    setNavTwo(true);
  }
  // active nav class 
  const router = useRouter();
  // console.log(router)
  let isActive = (route)=>{
    return route == router.pathname ?  "activeLink" : "";
  }
  return (
    <>
  
      <div className=" bg-white z-50 sticky top-0" style={{zIndex:1001}}>
        <div className=" mx-auto">
        {/* <div className=" mx-auto px-4 sm:px-6"> */}
        {/* <div className="max-w-7xl mx-auto px-4 sm:px-6"> */}
          <div className="flex px-4 md:px-16 justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10 lg:space-x-28">
            <div className="lg:w-auto lg:flex">
              <Link href="/" >
                <a className="flex">
                  <img className="h-8 w-auto sm:h-10" src="/images/farmgate_logo.svg" alt="farmgate logo"/>
                </a>
              </Link>
            </div>
            <div className="-mr-2 -my-2 md:hidden">
              <button type="button" className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" onClick={navToggle}>
                {/* <!-- Heroicon name: menu --> */}
                <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </button>
            </div>
            <nav className="hidden md:flex space-x-10">
              {/* button closes any navbar dropdown  */}
              <button onClick={closeNavDropdown} className={"cursor-default inset-0 w-full h-full fixed bg-none focus:border-none focus:bg-none focus:outline-none;" + ((navOne == false || navTwo == false) ? " flex" : " hidden")}></button>
              <div className="relative">
                {/* <!-- Item active: "text-gray-900", Item inactive: "text-gray-500" --> */}
                <button type="button" onClick = {()=>{setNavOne(!navOne); setNavTwo(true)}} className="text-gray-500 group inline-flex items-center space-x-2 text-base leading-6 font-normal focus:font-medium hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
                  <span>Join as</span>
                  {/* <!--
                    Heroicon name: chevron-down

                    Item active: "text-gray-600", Item inactive: "text-gray-400"
                  --> */}
                  {/* <svg className="text-gray-400 h-5 w-5 group-hover:text-gray-500 group-focus:text-gray-500 transition ease-in-out duration-150" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clipRule="evenodd" />
                  </svg> */}
                  <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14 0H0L7.25926 8L14 0Z" fill="#E69740"/>
                  </svg>

                </button>

                {/* <!--
                  'Solutions' flyout menu, show/hide based on flyout menu state.
                --> */}
                <div className={"absolute -ml-4 mt-3 transform px-2 w-auto max-w-md sm:px-0 lg:ml-0 lg:left-0" + (navOne ? " hidden dropdown_in" : " flex dropdown_out")}>
                {/* <div className={"absolute -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2" + (navOne ? " hidden dropdown_in" : " flex dropdown_out")}> */}
                  <div className="rounded-lg shadow-lg">
                    <div className="rounded-lg shadow-xs overflow-hidden">
                      <div className="z-20 relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8">
                        <a href="#" className="-m-3 p-3 flex items-start space-x-4 rounded-lg hover:bg-gray-50 transition ease-in-out duration-150">
                          {/* <!-- Heroicon name: chart-bar --> */}
                          <svg className="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
                          </svg>
                          <div className="space-y-1">
                            <p className="text-base leading-6 font-medium text-gray-900">
                              Analytics
                            </p>
                        
                          </div>
                        </a>
                        <a href="#" className="-m-3 p-3 flex items-start space-x-4 rounded-lg hover:bg-gray-50 transition ease-in-out duration-150">
                          {/* <!-- Heroicon name: cursor-click --> */}
                          <svg className="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122" />
                          </svg>
                          <div className="space-y-1">
                            <p className="text-base leading-6 font-medium text-gray-900">
                              Engagement
                            </p>
                           
                          </div>
                        </a>
                    
                      </div>
                
                    </div>
                  </div>
                </div>
              </div>
            
              <a href="#" className="text-base leading-6 font-normal focus:font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
              Regions
              </a>
              <a href="#" className="text-base leading-6 font-normal focus:font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
                About us
              </a>
              <a href="#" className="text-base leading-6 font-normal focus:font-medium text-gray-500 hover:text-gray-900 focus:outline-none focus:text-gray-900 transition ease-in-out duration-150">
                Contact us
              </a>
           
        
            </nav>
          </div>
        </div>

        {/* <!--
          Mobile menu, show/hide based on mobile menu state.

          Entering: "duration-200 ease-out"
            From: "opacity-0 scale-95"
            To: "opacity-100 scale-100"
          Leaving: "duration-100 ease-in"
            From: "opacity-100 scale-100"
            To: "opacity-0 scale-95"
        --> */}
  
        <div className={"absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden" + (navbarOpen ? " block menu_drop_in" : " hidden menu_drop_out")} >
          <div className="rounded-lg shadow-lg">
            <div className="rounded-lg shadow-xs bg-white divide-y-2 divide-gray-50">
              <div className="pt-5 pb-6 px-5 space-y-6">
                <div className="flex items-center justify-between">
                  <div>
                    <img className="h-8 w-auto" src="/images/farmgate_logo.svg" alt="farmgate logo"/>
                  </div>
                  <div className="-mr-2">
                    <button type="button" className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out" onClick={navToggle}>
                      {/* <!-- Heroicon name: x --> */}
                      <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                      </svg>
                    </button>
                  </div>
                </div>
                <div>
                  <nav className="grid gap-y-8">
                    <a href="#" className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                      {/* <!-- Heroicon name: chart-bar --> */}
                      <svg className="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
                      </svg>
                      <div className="text-base leading-6 font-medium text-gray-900">
                        Region
                      </div>
                    </a>
                    <a href="#" className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                      {/* <!-- Heroicon name: cursor-click --> */}
                      <svg className="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122" />
                      </svg>
                      <div className="text-base leading-6 font-medium text-gray-900">
                        About us
                      </div>
                    </a>
                    <a href="#" className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                      {/* <!-- Heroicon name: shield-check --> */}
                      <svg className="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z" />
                      </svg>
                      <div className="text-base leading-6 font-medium text-gray-900">
                        Contact us
                      </div>
                    </a>
                    <a href="#" className="-m-3 p-3 flex items-center space-x-3 rounded-md hover:bg-gray-50 transition ease-in-out duration-150">
                      {/* <!-- Heroicon name: view-grid --> */}
                      <svg className="flex-shrink-0 h-6 w-6 text-indigo-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
                      </svg>
                      <div className="text-base leading-6 font-medium text-gray-900">
                        Join as:
                      </div>
                    </a>
                  </nav>
                </div>
              </div>
              <div className="py-6 px-5 space-y-6">
                <div className="grid grid-cols-2 gap-y-4 gap-x-8">
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Farmer
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Input provider
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Enterprise
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Blog
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Help Center
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Guides
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Security
                  </a>
                  <a href="#" className="text-base leading-6 font-medium text-gray-900 hover:text-gray-700 transition ease-in-out duration-150">
                    Events
                  </a>
                </div>
                {/* <div className="space-y-6">
                  <span className="w-full flex rounded-md shadow-sm">
                    <a href="#" className="w-full flex items-center justify-center px-4 py-2 border border-transparent text-base leading-6 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition ease-in-out duration-150">
                      Sign up
                    </a>
                  </span>
                  <p className="text-center text-base leading-6 font-medium text-gray-500">
                    Existing customer?
                    <a href="#" className="text-indigo-600 hover:text-indigo-500 transition ease-in-out duration-150">
                      Sign in
                    </a>
                  </p>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
    
  )
}
